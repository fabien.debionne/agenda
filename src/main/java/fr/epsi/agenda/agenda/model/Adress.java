package fr.epsi.agenda.agenda.model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "adresses")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Adress extends ContactDetail{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    private final String PATTERN = "\n" +
            "^([1-9][0-9]*(?:-[1-9][0-9]*)*)[\\s,-]+(?:(bis|ter|qua)[\\s,-]+)?([\\w]+[\\-\\w]*)[\\s,]+([-\\w].+)$";

    @Override
    public boolean validate(String value) {
        boolean isValidate = value.matches(PATTERN);
        return isValidate;
    }
}
