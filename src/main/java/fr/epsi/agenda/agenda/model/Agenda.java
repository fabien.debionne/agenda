package fr.epsi.agenda.agenda.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.HashMap;
import java.util.List;

@Entity
@Table(name = "agendas")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Agenda {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(mappedBy = "name")
    private List<Contact> contacts;
}
