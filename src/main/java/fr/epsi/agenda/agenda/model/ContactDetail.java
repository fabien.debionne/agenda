package fr.epsi.agenda.agenda.model;

import jakarta.persistence.*;
import lombok.*;


@Entity
@Table(name = "contactDetail")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public abstract class ContactDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    @Column(length = 255)
    private String value;

    @ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
    @JoinColumn(name = "contact_id")
    private Contact owner;

    public abstract boolean validate(String value);




}
