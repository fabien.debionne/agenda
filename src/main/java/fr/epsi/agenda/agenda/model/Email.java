package fr.epsi.agenda.agenda.model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "emails")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Email extends ContactDetail{

    @jakarta.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    private final String PATTERN = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";

    @Override
    public boolean validate(String value) {
        return value.matches(PATTERN);
    }
}

