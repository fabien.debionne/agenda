package fr.epsi.agenda.agenda.model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "phones")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Phone extends ContactDetail{

    @jakarta.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    private final String PATTERN = "^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\s\\./0-9]*$";

    @Override
    public boolean validate(String value) {
        boolean isValidate = false;
        return isValidate;
    }
}

