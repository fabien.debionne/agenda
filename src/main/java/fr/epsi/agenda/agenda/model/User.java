package fr.epsi.agenda.agenda.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.util.List;

@Entity
@Table(name = "users")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    @Column(length = 30)
    private String login;

    @Column(length = 50)
    @Size.List({
            @Size(min = 12, message = "{validation.password.size.too_short}"),
            @Size(max = 50, message = "{validation.password.size.too_long}")
    })
    private String password;

    @Column(length = 255)
    private String jwt_token;

    @OneToMany(mappedBy = "user")
    private List<Agenda> agendas;
}
