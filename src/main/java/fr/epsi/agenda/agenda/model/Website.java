package fr.epsi.agenda.agenda.model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "websites")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Website extends ContactDetail{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    private final String PATTERN = "^https?:\\\\/\\\\/(?:www\\\\.)?[-a-zA-Z0-9@:%._\\\\+~#=]{1,256}\\\\.[a-zA-Z0-9()]{1,6}\\\\b(?:[-a-zA-Z0-9()@:%_\\\\+.~#?&\\\\/=]*)$";

    @Override
    public boolean validate(String value) {
        boolean isValidate = value.matches(PATTERN);
        return isValidate;
    }
}

