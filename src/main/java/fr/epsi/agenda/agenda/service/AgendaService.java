package fr.epsi.agenda.agenda.service;

import fr.epsi.agenda.agenda.model.Agenda;
import fr.epsi.agenda.agenda.model.User;

import java.util.List;

public interface AgendaService {

    List<Agenda> getAll();

    Agenda getById(Integer id);

    /**
     * Create a agenda
     * @param agenda
     * @return
     */
    Agenda create(Agenda agenda);

    void delete(Integer id);

    User update(Agenda agenda);
}
