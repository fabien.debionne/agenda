package fr.epsi.agenda.agenda.service;

import fr.epsi.agenda.agenda.model.Contact;
import fr.epsi.agenda.agenda.model.User;

import java.util.List;

public interface ContactService {

    List<Contact> getAll();

    Contact getById(Integer id);

    /**
     * Create a contact
     * @param contact
     * @return
     */
    Contact create(Contact contact);

    void delete(Integer id);

    Contact update(Contact contact);
}
