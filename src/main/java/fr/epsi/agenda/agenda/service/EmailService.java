package fr.epsi.agenda.agenda.service;

import fr.epsi.agenda.agenda.model.Contact;
import fr.epsi.agenda.agenda.model.Email;

import java.util.List;

public interface EmailService {

    List<Email> getAll();

    Contact getById(Integer id);

    /**
     * Create a email
     * @param email
     * @return
     */
    Email create(Email email);

    void delete(Integer id);

    Email update(Email email);
}
