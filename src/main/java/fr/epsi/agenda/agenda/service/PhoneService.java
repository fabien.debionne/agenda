package fr.epsi.agenda.agenda.service;

import fr.epsi.agenda.agenda.model.Contact;
import fr.epsi.agenda.agenda.model.Phone;

import java.util.List;

public interface PhoneService {

    List<Phone> getAll();

    Contact getById(Integer id);

    /**
     * Create a phone
     * @param phone
     * @return
     */
    Phone create(Phone phone);

    void delete(Integer id);

    Phone update(Phone phone);
}
