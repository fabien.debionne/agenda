package fr.epsi.agenda.agenda.service;

import fr.epsi.agenda.agenda.model.User;

import java.util.List;

public interface UserService {

    List<User> getAll();

    User getById(Integer id);

    /**
     * Create a user
     * @param user
     * @return
     */
    User create(User user);

    void delete(Integer id);

    User update(User user);

}
