package fr.epsi.agenda.agenda.service;

import fr.epsi.agenda.agenda.model.Contact;
import fr.epsi.agenda.agenda.model.Website;

import java.util.List;

public interface WebsiteService {

    List<Website> getAll();

    Contact getById(Integer id);

    /**
     * Create a website
     * @param website
     * @return
     */
    Website create(Website website);

    void delete(Integer id);

    Website update(Website website);
}
