package fr.epsi.agenda.agenda.service.impl;

import fr.epsi.agenda.agenda.model.User;
import fr.epsi.agenda.agenda.repository.UserRepository;
import fr.epsi.agenda.agenda.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> getAll() {
        return this.userRepository.findAll(Sort.by("login").ascending());
    }

    @Override
    public User getById(Integer id) {
        return userRepository
                .findById(id)
                .orElseThrow();
    }

    @Override
    public User create(User user) {
        return this.userRepository.save(user);
    }

    @Override
    public void delete(Integer id) {
        User userToDelete = this.getById(id);
        if (this.canDeleteUser(userToDelete)) {
            this.userRepository.delete(userToDelete);
        } else {
            System.out.println("Vous ne pouvez pas supprimer cet utilisateur car il a un agenda");;
        }
    }

    @Override
    public User update(User user) {
        User existingUser = this.getById(user.getId());
        existingUser.setLogin(user.getLogin());
        existingUser.setAgendas(user.getAgendas());
        return this.userRepository.save(existingUser);
    }

    private boolean canDeleteUser(User user) {
        return (null == user.getAgendas() || user.getAgendas().isEmpty());
    }
}


